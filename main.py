# This is a sample Python script.
import glob
import os.path
import random

import requests
import io
import base64
import gc
import json
import cv2
import math
import numpy as np
import matplotlib.pyplot as plt
import torch
from PIL import Image, PngImagePlugin
from rembg import remove
from segment_anything import SamPredictor, sam_model_registry


# indices of importand landmark for each class
CATEGORY_IMPORTANT_LANDMARKS = {
    1: [2, 6, 9, 10, 12, 15, 17, 20, 22, 23],  # short sleeve top
    2: [2, 6, 11, 12, 16, 19, 21, 24, 28, 29],  # long sleeve top
    3: [4, 6, 9, 10, 12, 15, 17, 20, 22, 23],  # short sleeve outwear
    4: [2, 6, 11, 12, 16, 19, 21, 24, 28, 29],  # long sleeve outwear
    5: [2, 6, 10, 12],  # vest
    6: [2, 6, 10, 12],  # sling
    7: [1, 3, 5, 6, 7, 8, 9],  # shorts
    8: [1, 3, 6, 7, 9, 11, 12],  # trousers
    9: [1, 3, 5, 7],  # skirt
    10: [2, 6, 9, 10, 12, 17, 19, 24, 26, 27],  # short sleeve dress
    11: [2, 6, 11, 12, 16, 21, 23, 28, 32, 33],  # long sleeve dress
    12: [2, 6, 7, 12, 14, 19],  # vest dress
    13: [2, 6, 7, 12, 14, 19]  # sling dress
}

# [width_min, width_max, height_min, height_max]
# background max of original data is 1024 x 1984
# conveyor belt width is about 70 pixels away from image border
CATEGORY_MIN_MAX_DIMENSIONS = {
    1: [650, 880, 700, 1100],  # short sleeve top
    2: [650, 880, 700, 1100],  # long sleeve top
    3: [650, 880, 700, 1100],  # short sleeve outwear
    4: [650, 880, 700, 1100],  # long sleeve outwear
    5: [650, 880, 700, 1100],  # vest
    6: [650, 880, 700, 1100],  # sling
    7: [650, 880, 400, 800],  # shorts
    8: [650, 880, 800, 1200],  # trousers
    9: [650, 880, 400, 800],  # skirt
    10: [650, 880, 800, 1200],  # short sleeve dress
    11: [650, 880, 800, 1200],  # long sleeve dress
    12: [650, 880, 700, 1100],  # vest dress
    13: [650, 880, 700, 1100]  # sling dress
}


def main(inpaint: bool = True, segment_dir: str = f"/home/nico/Downloads/DeepFashion/deepfashion2/train{os.sep}outputs{os.sep}", occlusion_limit: int = 2,
         debug: bool = False, plot_landmarks: bool = False, plot_sam_segmentation: bool = False,
         plot_sam_postprocessing: bool = False, plot_response_without_prompts: bool = False, sam_type: str = "vit_h"):
    fns = load_filenames()

    # load segment anything model
    if sam_type == "vit_h":
        sam = sam_model_registry["vit_h"](checkpoint=f".{os.sep}models{os.sep}sam_vit_h_4b8939.pth")
    elif sam_type == "vit_l":
        sam = sam_model_registry["vit_l"](checkpoint=f".{os.sep}models{os.sep}sam_vit_l_0b3195.pth")
    elif sam_type == "vit_b":
        sam = sam_model_registry["vit_b"](checkpoint=f".{os.sep}models{os.sep}sam_vit_b_01ec64.pth")
    else:
        print(f"sam type '{sam_type}' not available.")
        import sys
        sys.exit()

    device = "cuda"
    sam.to(device=device)
    predictor = SamPredictor(sam)

    iter = 1
    # iterate over every single image
    for fn in fns:
        img = cv2.imread(fn)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        # get the skin segmentations done by another model
        fn_skin_segmentation = f"/home/nico/Downloads/DeepFashion/deepfashion2/train/skin_segmentations/masks{os.sep}skin_{os.path.basename(fn)}"
        img_skin = cv2.imread(fn_skin_segmentation)
        img_skin = cv2.cvtColor(img_skin, cv2.COLOR_RGB2GRAY)

        # get contours of skin
        thresh = cv2.threshold(img_skin, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]
        cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        total = 0

        # find centers of the skin regions large enough (more than 20 pixels)
        skin_center_spots = []
        for c in cnts:
            x, y, w, h = cv2.boundingRect(c)
            center = [x + w // 2, y + h // 2]
            mask = np.zeros(img.shape, dtype=np.uint8)
            #cv2.fillPoly(mask, [c], [255, 255, 255])
            mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
            pixels = cv2.countNonZero(mask)
            total += pixels
            #cv2.putText(img, '{}'.format(pixels), (x, y - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2)

            if pixels > 20:
                skin_center_spots.append(center)

        ##cv2.imwrite(f'thresh_{iter}.jpg', thresh)
        #cv2.imwrite(f'image_{iter}.jpg', img)
        iter += 1

        predictor.set_image(img)

        # load the annotation file of the image
        with open(fn.replace("image", "annos").replace("jpg", "json"), "r") as f:
            data = json.load(f)

        # get the annotations of every clothing object
        for k, v in data.items():
            if k.startswith("item"):
                if data[k]['occlusion'] > occlusion_limit:
                    print(k, "skipped. To much occlusion.")
                    continue

                seg = v["segmentation"]
                bbox = v["bounding_box"]
                landmarks = v["landmarks"]
                input_box = np.array(bbox)

                if plot_landmarks:
                    _img = img
                    for lm in range(len(landmarks)//3):
                        x = landmarks[lm * 3]
                        y = landmarks[lm * 3 + 1]
                        v = landmarks[lm * 3 + 2]
                        if v == 0:
                            # red
                            color = [255, 0, 0]
                        elif v == 1:
                            # yellow
                            color = [255, 255, 0]
                        elif v == 2:
                            # green
                            color = [0, 255, 0]
                        else:
                            print("Error")
                        cv2.circle(_img, (x, y), 5, color, -1)

                    _img = cv2.cvtColor(_img, cv2.COLOR_RGB2BGR)
                    cv2.imwrite(f"./{os.path.basename(fn)[:-4]}_{k}.png", _img)

                # get relevant lms for specific item category
                important_landmarks_idx = CATEGORY_IMPORTANT_LANDMARKS[v["category_id"]]
                # visibility information is at every 3rd position in landmark labelings (x1, y1, v1, ...)
                landmark_visibilities = landmarks[2::3]

                # adapt from formal to python indexing (1 -> 0, ...)
                important_landmarks_idx = [x-1 for x in important_landmarks_idx]
                important_landmarks_visibilities = [landmark_visibilities[idx] for idx in important_landmarks_idx]

                if important_landmarks_visibilities.count(0) / len(important_landmarks_visibilities) > 0.1:
                    print(k, "skipped. To much important landmarks unlabeled.")
                    continue

                share_visible_landmarks = (landmark_visibilities.count(1) + landmark_visibilities.count(2)) / len(landmark_visibilities)
                if share_visible_landmarks < 0.6:
                    print(k, "skipped. To much landmarks unlabeled.")
                    continue

                if debug:
                    print(f"share visible landmarks = {share_visible_landmarks}")

                stencil = np.zeros(img.shape).astype(img.dtype)
                stencil.fill(255)
                stencil_inverted = np.zeros(img.shape).astype(img.dtype)
                mask = np.zeros(img.shape).astype(img.dtype)

                for poly in seg:
                    # convert polygon to coordinate pairs
                    pairs = []
                    for i in range(0, len(poly), 2):
                        pairs.append([int(poly[i]), int(poly[i + 1])])
                    points = np.array(pairs)
                    cv2.fillPoly(stencil, pts=[points], color=(0, 0, 0))
                    cv2.fillPoly(stencil_inverted, pts=[points], color=(255, 255, 255))

                if debug:
                    plt.figure(figsize=(10, 10))
                    plt.title("stencil")
                    plt.imshow(stencil)
                    plt.axis('off')
                    plt.show()

                    plt.figure(figsize=(10, 10))
                    plt.title("stencil inverted")
                    plt.imshow(stencil_inverted)
                    plt.axis('off')
                    plt.show()

                # inference of segmentation using the bbox annotation and skin centers as a counternotice
                if len(skin_center_spots) > 0:
                    input_point = np.array(skin_center_spots)
                    input_label = np.array([0 for scs in skin_center_spots])

                    masks, scores, _ = predictor.predict(
                        point_coords=input_point,
                        point_labels=input_label,
                        box=input_box[None, :],
                        multimask_output=True,
                    )
                else:
                    masks, scores, _ = predictor.predict(
                        point_coords=None,
                        point_labels=None,
                        box=input_box[None, :],
                        multimask_output=True,
                    )
                best_score_index = scores.argmax()

                # identify the best segmentation by calculating the iou between prediction and landmark annotation
                best_segmentation_iou = True
                use_ratio = False
                if best_segmentation_iou:
                    best_mask_index = 0
                    best_iou = 0.

                    # print(f"found {len(masks)} masks")
                    masks = masks.astype(np.uint8)

                    # fill every region with the annotation by using the segmentations (sometimes multiple)
                    segmentation_mask = np.zeros(masks[0].shape).astype(masks.dtype)
                    for s in seg:
                        s = np.array(s).reshape((-1, 1, 2)).astype(int)
                        cv2.drawContours(segmentation_mask, [s], -1, (1), thickness=cv2.FILLED)

                    for j in range(0, 3):

                        iou = calc_iou(segmentation_mask, masks[j])
                        # print(f"iou j={j} is {iou}")

                        if iou > best_iou:
                            best_iou = iou
                            best_mask_index = j
                    best_mask = masks[best_mask_index]
                elif use_ratio:
                    m = mask.copy()
                    best_ratio_diff = 0
                    best_inside_area_ratio = 0
                        
                    m[masks[j]] = (255, 255, 255)
                    outside_area = cv2.bitwise_and(stencil, m)
                    outside_area_ratio = np.sum(outside_area == 255) / np.sum(stencil_inverted == 255)
    
                    inside_area = cv2.bitwise_and(stencil_inverted, m)
                    inside_area_ratio = np.sum(inside_area == 255) / np.sum(stencil_inverted == 255)
                        
    
                    ratio_diff = inside_area_ratio - outside_area_ratio
                    if ratio_diff > best_ratio_diff:
                        best_ratio_diff = ratio_diff
                        best_inside_area_ratio = inside_area_ratio
                        best_mask_indx = j
                    if debug:
                        print(f"BEST IOU = {best_iou}")
                    best_mask = masks[best_mask_index]
                else:
                    least_components = 999
                    for c in range(0, 3):
                        cur_mask = masks[c].astype(np.uint8) * 255
                        cnts = cv2.findContours(cur_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
                        n_cnts = len(cnts)
                        if n_cnts < least_components and n_cnts > 0:
                            least_components = n_cnts
                            best_mask_index = c
                    best_mask = masks[best_mask_index]

                if plot_sam_segmentation:
                    plt.figure(figsize=(4.68, 7.02), dpi=100)
                    plt.imshow(img)
                    show_mask(best_mask, plt.gca())
                    show_box(input_box, plt.gca())
                    plt.axis('off')
                    plt.savefig(f"./{os.path.basename(fn)[:-4]}_{k}_sam_best_mask.jpg", bbox_inches='tight', pad_inches=0)
                    plt.show()

                if debug:
                    plt.figure(figsize=(10, 10))
                    plt.title("img and best prediction overlay")
                    plt.imshow(img)
                    show_mask(best_mask, plt.gca())
                    show_box(input_box, plt.gca())
                    plt.axis('off')
                    plt.show()

                    plt.figure(figsize=(10, 10))
                    plt.title("img and best prediction overlay")
                    plt.imshow(stencil)
                    show_mask(best_mask, plt.gca())
                    show_box(input_box, plt.gca())
                    plt.axis('off')
                    plt.show()

                    plt.figure(figsize=(10, 10))
                    plt.title("best mask")
                    plt.imshow(best_mask)
                    plt.axis('off')
                    plt.show()

                # crop img and mask
                img_bbox = img[bbox[1]: bbox[3], bbox[0]:bbox[2]]
                best_mask = best_mask[bbox[1]: bbox[3], bbox[0]:bbox[2]]
                stencil_inverted = stencil_inverted[bbox[1]: bbox[3], bbox[0]:bbox[2]]

                img_no_alpha = img_bbox

                # Apply the mask and add an alpha channel
                alpha = 255 * np.ones((img_bbox.shape[0], img_bbox.shape[1]), dtype=img_bbox.dtype)

                inverse_mask = np.logical_not(best_mask)

                img_alpha = cv2.merge((img_bbox, alpha))
                img_alpha[inverse_mask] = (0, 0, 0, 0)

                if debug:
                    plt.figure(figsize=(10, 10))
                    plt.title("img_alpha")
                    plt.imshow(img_alpha)
                    plt.axis('off')
                    plt.show()

                # morphological operations to smoothen the predicted segmentation
                k_size = 5
                k_fill1 = 5
                k_fill2 = 20
                threshold = 50  # at least 50 pixels in each extracted region
                kernel_erosion = np.ones((k_size, k_size), np.uint8)
                kernel_dilation1 = np.ones((k_fill1, k_fill1), np.uint8)
                kernel_dilation2 = np.ones((k_size + k_fill2, k_size + k_fill2), np.uint8)

                # mask = np.expand_dims(masks[best_mask_index], -1)

                # inpaint if necessary (iou < threshold)
                iou_threshold = 0.95
                if inpaint & (best_iou < iou_threshold):
                    gc.collect()
                    torch.cuda.empty_cache()

                    mask = np.zeros(img_no_alpha.shape).astype(img_no_alpha.dtype)
                    mask[inverse_mask] = (255, 255, 255)

                    if debug:
                        plt.figure(figsize=(10, 10))
                        plt.title("mask")
                        plt.imshow(mask)
                        plt.axis('off')
                        plt.show()

                    stencil_inverted = cv2.dilate(stencil_inverted, kernel_dilation1, iterations=1)

                    w, h = stencil_inverted.shape[:2]
                    stencil_inverted[0, :, :] = (0, 0, 0)
                    stencil_inverted[:, 0, :] = (0, 0, 0)
                    stencil_inverted[w - 1, :, :] = (0, 0, 0)
                    stencil_inverted[:, h - 1, :] = (0, 0, 0)

                    if debug:
                        plt.figure(figsize=(10, 10))
                        plt.title("stencil inverted")
                        plt.imshow(stencil_inverted)
                        plt.axis('off')
                        plt.show()

                    stencil_inverted = cv2.erode(stencil_inverted, kernel_erosion, iterations=7)

                    # Mask everything that's not in one of the polygons
                    mask = cv2.bitwise_and(stencil_inverted, mask)
                    mask_init = mask

                    gray = cv2.cvtColor(mask, cv2.COLOR_RGB2GRAY)

                    if debug:
                        plt.figure(figsize=(10, 10))
                        plt.title("cleaned up mask")
                        plt.imshow(mask)
                        show_box(input_box, plt.gca())
                        plt.axis('off')
                        plt.show()

                    (numLabels, labels, stats, centroids) = cv2.connectedComponentsWithStats(gray, connectivity=4)
                    for i in range(numLabels):
                        if stats[i, cv2.CC_STAT_AREA] < threshold:
                            mask[labels == i] = (0, 0, 0)

                    mask = cv2.dilate(mask, kernel_dilation2, iterations=1)
                    # cv2.imwrite(os.path.join(segmentedDir, mask_filename), mask)

                    """
                    # crop to bounding box
                    x1 = input_box[0]
                    x2 = input_box[2]
                    y1 = input_box[1]
                    y2 = input_box[3]
                    img_alpha = img_alpha[y1:y2, x1:x2]
                    mask = mask[y1:y2, x1:x2]
                    """

                    if plot_sam_postprocessing:
                        # plot sam mask
                        plt.figure(figsize=(4.68, 7.02), dpi=100)
                        _best_mask = cv2.cvtColor(best_mask, cv2.COLOR_GRAY2RGB) * 255
                        plt.imshow(_best_mask)
                        plt.axis('off')
                        plt.savefig(f"./{os.path.basename(fn)[:-4]}_{k}_postprocessing_sam_best_mask.jpg", bbox_inches='tight',
                                    pad_inches=0)
                        plt.show()

                        # plot segmentation mask
                        plt.figure(figsize=(4.68, 7.02), dpi=100)
                        plt.imshow(stencil_inverted)
                        plt.axis('off')
                        plt.savefig(f"./{os.path.basename(fn)[:-4]}_{k}_postprocessing_segmentation_mask.jpg",
                                    bbox_inches='tight',
                                    pad_inches=0)
                        plt.show()

                        # plot union without cut
                        plt.figure(figsize=(4.68, 7.02), dpi=100)
                        plt.imshow(mask_init)
                        plt.axis('off')
                        plt.savefig(f"./{os.path.basename(fn)[:-4]}_{k}_postprocessing_union_without_cut.jpg",
                                    bbox_inches='tight',
                                    pad_inches=0)
                        plt.show()

                        # plot post processed
                        plt.figure(figsize=(4.68, 7.02), dpi=100)
                        plt.imshow(mask)
                        plt.axis('off')
                        plt.savefig(f"./{os.path.basename(fn)[:-4]}_{k}_postprocessing_output.jpg",
                                    bbox_inches='tight',
                                    pad_inches=0)
                        plt.show()

                    # TODO fill with sampled color from rest of object
                    img_alpha = fill_occlusion_with_color(img_alpha, mask)

                    url = "http://127.0.0.1:7860"

                    opt = requests.get(url=f'{url}/sdapi/v1/options')
                    response = opt.json()
                    response['sd_model_checkpoint'] = 'sd-v1-5-inpainting.ckpt'
                    requests.post(url=f'{url}/sdapi/v1/options', json=response)
                    if debug:
                        plt.figure(figsize=(10, 10))
                        plt.title("inpainting input img")
                        plt.imshow(img_alpha)
                        plt.axis('off')
                        plt.show()

                        plt.figure(figsize=(10, 10))
                        plt.title("inpainting input mask")
                        plt.imshow(mask)
                        plt.axis('off')
                        plt.show()

                    _, buffer = cv2.imencode('.png', img_alpha)
                    encoded_image = base64.b64encode(buffer).decode('utf-8')
                    _, buffer = cv2.imencode('.png', mask)
                    encoded_mask = base64.b64encode(buffer).decode('utf-8')

                    height, width = img_alpha.shape[:2]

                    payload = {
                        "init_images": [
                            encoded_image
                        ],
                        "resize_mode": 3,  # latent upscale
                        "denoising_strength": 0.8,  # Setting it to 0 changes nothing. Setting to 1 you got an unrelated image. was 0.9
                        "image_cfg_scale": 15,  # 1 – Mostly ignore your prompt 3 – Be more creative 7 – A good balance between following the prompt and freedom 15 – Adhere more to the prompt. 30 – Strictly follow the prompt.
                        "mask": encoded_mask,
                        "mask_blur": 1,  # blur inpainting in to image - LOW as high values make the inpainting invisible
                        "inpainting_fill": 1,  # use 0 to fill with random pixel gathered from the bbox, 2 with random noise
                        # masked content: 0 - fill, 1 - original, 2 - latent noise, 3 - latent nothing
                        "inpaint_full_res": True,
                        "inpaint_full_res_padding": 48,  # 4 before
                        "inpainting_mask_invert": 0,
                        "initial_noise_multiplier": 0,
                        "prompt": data[k]["category_name"],
                        "seed": -1,
                        "subseed": -1,
                        "subseed_strength": 0,
                        "seed_resize_from_h": -1,
                        "seed_resize_from_w": -1,
                        "sampler_name": "DPM++ SDE Karras",
                        "batch_size": 1,
                        "n_iter": 1,
                        "steps": 15,
                        "cfg_scale": 15,  # 8
                        "width": width,
                        "height": height,
                        "restore_faces": False,
                        "tiling": False,
                        "do_not_save_samples": False,
                        "do_not_save_grid": False,
                        "negative_prompt": "hand, fingers, skin, human, man, woman",
                        # "hand, fingers, skin, human, man, woman, hair, face",
                        "eta": 0,
                        "s_churn": 0,
                        "s_tmax": 0,
                        "s_tmin": 0,
                        "s_noise": 1,
                        "override_settings": {},
                        "override_settings_restore_afterwards": True,
                        "sampler_index": "DPM++ SDE Karras",  # DDIM
                        "include_init_images": False,
                        "send_images": True,
                        "save_images": False,
                        "alwayson_scripts": {}
                    }

                    payload_wo_prompts = {
                        "init_images": [
                            encoded_image
                        ],
                        "resize_mode": 3,  # latent upscale
                        "denoising_strength": 0.8,
                        # Setting it to 0 changes nothing. Setting to 1 you got an unrelated image. was 0.9
                        "image_cfg_scale": 15,
                        # 1 – Mostly ignore your prompt 3 – Be more creative 7 – A good balance between following the prompt and freedom 15 – Adhere more to the prompt. 30 – Strictly follow the prompt.
                        "mask": encoded_mask,
                        "mask_blur": 1,
                        # blur inpainting in to image - LOW as high values make the inpainting invisible
                        "inpainting_fill": 0,
                        # use 0 to fill with random pixel gathered from the bbox, 2 with random noise
                        # masked content: 0 - fill, 1 - original, 2 - latent noise, 3 - latent nothing
                        "inpaint_full_res": True,
                        "inpaint_full_res_padding": 48,  # 4 before
                        "inpainting_mask_invert": 0,
                        "initial_noise_multiplier": 0,
                        "prompt": "hand, skin",
                        "seed": -1,
                        "subseed": -1,
                        "subseed_strength": 0,
                        "seed_resize_from_h": -1,
                        "seed_resize_from_w": -1,
                        "sampler_name": "DPM++ SDE Karras",
                        "batch_size": 1,
                        "n_iter": 1,
                        "steps": 15,
                        "cfg_scale": 15,  # 8
                        "width": width,
                        "height": height,
                        "restore_faces": False,
                        "tiling": False,
                        "do_not_save_samples": False,
                        "do_not_save_grid": False,
                        "negative_prompt": "",
                        # "hand, fingers, skin, human, man, woman, hair, face",
                        "eta": 0,
                        "s_churn": 0,
                        "s_tmax": 0,
                        "s_tmin": 0,
                        "s_noise": 1,
                        "override_settings": {},
                        "override_settings_restore_afterwards": True,
                        "sampler_index": "DPM++ SDE Karras",  # DDIM
                        "include_init_images": False,
                        "send_images": True,
                        "save_images": False,
                        "alwayson_scripts": {}
                    }

                    response = requests.post(url=f'{url}/sdapi/v1/img2img', json=payload)
                    if plot_response_without_prompts:
                        response_wo_prompts = requests.post(url=f'{url}/sdapi/v1/img2img', json=payload_wo_prompts)
                        r_wo_prompts = response_wo_prompts.json()
                        try:
                            i = r_wo_prompts['images'][0]
                            inpainted_image_wo_prompts = Image.open(io.BytesIO(base64.b64decode(i.split(",", 1)[0])))

                            png_payload_wo_prompts = {
                                "image": "data:image/png;base64," + i
                            }

                            response2_wo_prompts = requests.post(url=f'{url}/sdapi/v1/png-info', json=png_payload_wo_prompts)
                            pnginfo = PngImagePlugin.PngInfo()
                            pnginfo.add_text("parameters", response2_wo_prompts.json().get("info"))
                            inpainted_image_wo_prompts = remove(inpainted_image_wo_prompts)
                            inpainted_image_wo_prompts = np.array(inpainted_image_wo_prompts)
                            inpainted_image_wo_prompts = delete_alpha_border(inpainted_image_wo_prompts)
                            # inpainted_image = apply_image_to_conveyor(inpainted_image, data, k)
                            inpainted_filename_alpha_wo_prompts = os.path.basename(fn).replace(".jpg", "") + k + "_inpainted_wo_prompts" + ".png"
                            cv2.imwrite(os.path.join(segment_dir, inpainted_filename_alpha_wo_prompts), inpainted_image_wo_prompts)
                        except:
                            print("Stable-Diffusion error. -> Skipped")

                    if debug:
                        print(f'{url}/sdapi/v1/img2img')

                    r = response.json()

                    try:
                        i = r['images'][0]
                        inpainted_image = Image.open(io.BytesIO(base64.b64decode(i.split(",", 1)[0])))

                        png_payload = {
                            "image": "data:image/png;base64," + i
                        }

                        response2 = requests.post(url=f'{url}/sdapi/v1/png-info', json=png_payload)
                        pnginfo = PngImagePlugin.PngInfo()
                        pnginfo.add_text("parameters", response2.json().get("info"))
                        inpainted_image = remove(inpainted_image)
                        inpainted_image = np.array(inpainted_image)
                        if debug:
                            plt.figure(figsize=(10, 10))
                            plt.title("inpaint return")
                            plt.imshow(inpainted_image)
                            plt.axis('off')
                            plt.show()

                        inpainted_image = delete_alpha_border(inpainted_image)
                        #inpainted_image = apply_image_to_conveyor(inpainted_image, data, k)
                        inpainted_filename_alpha = os.path.basename(fn).replace(".jpg", "") + k + "_inpainted" + ".png"
                        cv2.imwrite(os.path.join(segment_dir, inpainted_filename_alpha), inpainted_image)
                    except:
                        print("Stable-Diffusion error. -> Skipped")
                    if debug:
                        plt.figure(figsize=(10, 10))
                        plt.title("result")
                        plt.imshow(inpainted_image)
                        plt.axis('off')
                        plt.show()

                        # continue Y>
                else:
                    # image_with_border = apply_image_to_conveyor(img_alpha, data, k)
                    image_filename_alpha = os.path.basename(fn).replace(".jpg", "") + k + ".png"
                    cv2.imwrite(os.path.join(segment_dir, image_filename_alpha), img_alpha)


def load_filenames(test: bool = False) -> []:
    fns = glob.glob(f"{os.sep}home{os.sep}nico{os.sep}Downloads{os.sep}DeepFashion{os.sep}deepfashion2{os.sep}train{os.sep}image{os.sep}*.jpg")
    # /home/nico/Downloads/DeepFashion/deepfashion2/validation

    fns = sorted(fns)
    count = False
    if count:
        item_count = 0
        for fn in fns:
            # load the annotation file of the image
            with open(fn.replace("image", "annos").replace("jpg", "json"), "r") as f:
                data = json.load(f)

            # get the annotations of every clothing object
            for k, v in data.items():
                if k.startswith("item"):
                    item_count += 1
        print(f"image object count = {item_count}")

    if test:
        fns = fns[:50]
        #fns = fns[10:12]
    fns = fns[151140:]
    print(f"len fns = {len(fns)}")

    return fns


def show_mask(mask, ax, random_color=False):
    if random_color:
        color = np.concatenate([np.random.random(3), np.array([0.6])], axis=0)
    else:
        color = np.array([30 / 255, 144 / 255, 255 / 255, 0.6])
    h, w = mask.shape[-2:]
    mask_image = mask.reshape(h, w, 1) * color.reshape(1, 1, -1)
    ax.imshow(mask_image)


def show_points(coords, labels, ax, marker_size=375):
    pos_points = coords[labels == 1]
    neg_points = coords[labels == 0]
    ax.scatter(pos_points[:, 0], pos_points[:, 1], color='green', marker='*', s=marker_size, edgecolor='white',
               linewidth=1.25)
    ax.scatter(neg_points[:, 0], neg_points[:, 1], color='red', marker='*', s=marker_size, edgecolor='white',
               linewidth=1.25)


def show_box(box, ax):
    x0, y0 = box[0], box[1]
    w, h = box[2] - box[0], box[3] - box[1]
    ax.add_patch(plt.Rectangle((x0, y0), w, h, edgecolor='green', facecolor=(0, 0, 0, 0), lw=2))


def skin_classification(img):
    pass


def count_masks_with_size():
    pass


def calc_iou(mask1, mask2):
    mask1_area = np.count_nonzero(mask1 == 1)       # I assume this is faster as mask1 == 1 is a bool array
    mask2_area = np.count_nonzero(mask2 == 1)
    intersection = np.count_nonzero(np.logical_and(mask1, mask2))
    iou = intersection/(mask1_area+mask2_area-intersection)
    return iou


def get_rotation_vector(category, landmarks_vectors):
    # Select landmarks depending on the clothing
    # Look at the DeepFashion2 repro for more info
    category1 = ['shorts', 'trousers', 'skirt']
    category2 = ['long sleeve top', 'short sleeve top', 'long sleeve outwear', 'vest',
                 'sling', 'short sleeve dress', 'long sleeve dress', 'vest dress', 'sling dress']
    category3 = ['short sleeve outwear']
    if category1.__contains__(category):
        v1 = 1
        v2 = 3
    elif category2.__contains__(category):
        v1 = 2
        v2 = 6
    elif category3.__contains__(category):
        v1 = 4
        v2 = 6
    else:
        raise Exception("Not a valid category:", category)
    return landmarks_vectors[v2 - 1] - landmarks_vectors[v1 - 1]


def delete_alpha_border(image):
    y, x = image[:, :, 3].nonzero()
    return image[np.min(y):np.max(y), np.min(x):np.max(x)]


def fill_occlusion_with_color(img, mask, debug: bool = False):
    # maybe you patches
    pixels = img.flatten().reshape((-1, 4))

    pixel_set, pixel_distribution = np.unique(pixels, axis=0, return_counts=True)

    # remove background ([0 0 0 0])
    pixel_set = pixel_set[1:-1]
    pixel_distribution = pixel_distribution[1:-1]

    # distribution needs to sum up to 1
    pixel_distribution = pixel_distribution / pixel_distribution.sum()

    indices_mask = np.where(np.all(mask == [255, 255, 255], axis=-1))

    for i, j in zip(indices_mask[0], indices_mask[1]):
        rand_indice = np.random.choice(len(pixel_set), 1, p=pixel_distribution)
        img[i, j] = pixel_set[rand_indice]

    if debug:
        randint = np.random.randint(low=0, high=100000)
        cv2.imwrite(f"C:{os.sep}Arbeit{os.sep}maskfill{os.sep}{randint}.png", img)

    return img


def apply_image_to_conveyor(image, data, item, height: int, rotate: bool = False, showImg: bool = False):
    landmarks = data[item]["landmarks"]
    landmarks_vectors = []
    for i in range(0, len(landmarks), 3):
        landmarks_vectors.append(np.array([landmarks[i], landmarks[i + 1]]))
    category = data[item]["category_name"]
    rotation_vector = get_rotation_vector(category, landmarks_vectors)

    files = [f for f in os.listdir(f".{os.sep}background_snippets{os.sep}")]
    #print(random.randint(low=0, high=len(files)))
    random_background_img = files[np.random.randint(low=0, high=len(files))]
    background_image = cv2.imread(f".{os.sep}background_snippets{os.sep}{random_background_img}")
    #background_image = cv2.imread(f"C:\\Users\\Nico\\PycharmProjects\\deepfashiontwo\\background_snippets\\{random_background_img}")
    _background_image = background_image
    for _ in range((height-4)//4):
        background_image = np.vstack((background_image, _background_image))
    background_height, background_width = background_image.shape[:2]
    image_height, image_width = image.shape[:2]

    # factor = int(background_width / image_width)
    if rotate:
        angle = math.degrees(math.atan2(rotation_vector[1], rotation_vector[0]))
        if abs(angle) > 90:
            angle -= 180

        center = (image_height / 2, image_width / 2)
        matrix = cv2.getRotationMatrix2D(center, angle, 1)

        rotated_box = cv2.boxPoints((center, (image_width, image_height), angle))
        rotated_box = np.intp(rotated_box)

        _, _, w, h = cv2.boundingRect(rotated_box)
        matrix[0, 2] += (w - image_width) / 2
        matrix[1, 2] += (h - image_height) / 2

        foreground_image = cv2.warpAffine(image, matrix, (w, h))
        if showImg:
            cv2.imshow('Rotated', foreground_image)
    else:
        foreground_image = image

    foreground_image = delete_alpha_border(foreground_image)

    foreground_height, foreground_width = foreground_image.shape[:2]
    x = background_width // 2 - foreground_width // 2
    y = background_height // 2 - foreground_height // 2

    roi = background_image[y:y + foreground_height, x:x + foreground_width]
    alpha = foreground_image[:, :, 3] / 255.0

    foreground_pixels = foreground_image[:, :, :3] * np.expand_dims(alpha, axis=2)

    roi_pixels = roi * np.expand_dims(1 - alpha, axis=2)
    background_image[y:y + foreground_height, x:x + foreground_width] = np.uint8(foreground_pixels + roi_pixels)
    # background_image = background_image[0:foreground_height + y, 0:background_width]

    # Display the resulting image
    if showImg:
        cv2.imshow('Result', background_image)
        cv2.waitKey(0)

    return background_image


def apply_images_to_conveyor_belt(dataset_dir: str, scale_factor: int = 9, upscale_size: int = 224*7):
    len_dirname = len(dataset_dir)
    for fn in glob.glob(f"{dataset_dir}*.png"):
        with open(fn.replace("outputs", "annos")[:len_dirname+4] + ".json", "r") as f:
            data = json.load(f)

        for k, v in data.items():
            if k.startswith("item") and k in fn:
                img = cv2.imread(fn, flags=cv2.IMREAD_UNCHANGED)
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGBA)

                category_id = data[k]['category_id']
                category_name = data[k]['category_name'].replace(" ", "_")

                w_min, w_max, h_min, h_max = CATEGORY_MIN_MAX_DIMENSIONS[category_id]
                h, w, _ = img.shape

                scf_w_min = w_min / w
                scf_w_max = w_max / w
                scf_h_min = h_min / h
                scf_h_max = h_max / h
                if scf_h_max < scf_w_min or scf_w_max < scf_h_min:
                    continue
                scf = random.uniform(max(scf_w_min, scf_h_min), min(scf_w_max, scf_h_max))

                img = cv2.resize(img, None, fx=scf, fy=scf, interpolation=cv2.INTER_CUBIC)

                inpainted_image = apply_image_to_conveyor(img, data, k, height=upscale_size)
                inpainted_image = cv2.cvtColor(inpainted_image, cv2.COLOR_RGB2BGR)
                cv2.imwrite(f"{os.path.basename(fn)[:-4]}_{k}_conveyor_belt.png", inpainted_image)
                continue

                output_img = np.zeros((upscale_size, upscale_size, 3))
                output_img[:, upscale_size//2-512:upscale_size//2+512, :] = inpainted_image
                output_img = cv2.resize(output_img, (upscale_size//scale_factor, upscale_size//scale_factor), interpolation=cv2.INTER_AREA)

                img_id = os.path.basename(fn)[:11]
                path = f"{dataset_dir}{category_name}{os.sep}{img_id}.png".replace("outputs", "conveyor_belt")
                os.makedirs(os.path.dirname(path), exist_ok=True)
                cv2.imwrite(path, output_img)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
    #apply_images_to_conveyor_belt("/home/nico/Downloads/DeepFashion/deepfashion2/train/outputs/")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
